
function keys(obj) {
    // Retrieve all the names of the object's properties.
    // Return the keys as strings in an array.
    // Based on http://underscorejs.org/#keys
    if(typeof obj !=='object'  || obj==null || Array.isArray(obj)){
        throw new Error("Invalid arguments")
    }
    else{if(Object.entries(obj).length==0 ){
            return 'you entered empty object';
        }
        let arr=[];

        for(let key in obj){
        
            arr.push(key);
        
        }return arr;
    }
}


module.exports=keys;
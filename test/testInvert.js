const invert=require("../invert");

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
try{
let result=invert(testObject);

console.log("\nOlder object:\n");

console.log(testObject);

console.log("\ninverted object:\n");

console.log(result);
}catch(err){console.log(err.message)}
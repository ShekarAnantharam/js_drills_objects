
function values(obj) {
    // Return all of the values of the object's own properties.
    // Ignore functions
    // http://underscorejs.org/#values
    //console.log(obj)
   
    if(typeof obj !=='object'  || obj==null ||obj=='undefined'|| Array.isArray(obj)){
        throw new Error("Invalid arguments")
    }
    else{
        if(Object.entries(obj).length==0 ){
             return 'you entered empty object'
        }
        else{   
                let arr=[]; 
            
                for(let key in obj){
                
                    arr.push(obj[key]);
                
                }
            
            return arr;
        }
    }

}

module.exports=values;
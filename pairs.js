
function pairs(obj) {
    // Convert an object into a list of [key, value] pairs.
    // http://underscorejs.org/#pairs
    if(typeof obj !=='object'  || obj==null || Array.isArray(obj)){
        throw new Error("Invalid arguments")
    }
    else{if(Object.entries(obj).length==0 ){
          return 'you entered empty object';
        }
        let arr=[];
        
        for(let key in obj){

            arr.push([key,obj[key]]);

        } 
        return arr;
    }
}
module.exports=pairs;
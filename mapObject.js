function mapObject(obj, cb) {
    // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
    // http://underscorejs.org/#mapObject
 
    if(typeof obj !=='object'  || obj==null || Array.isArray(obj)){
        throw new Error("Invalid arguments")
    }
    else{if(Object.entries(obj).length==0 ){
            return 'you entered empty object';
        }
        
        for(let key in obj){
        
            cb(obj,key);
        
        }return obj;
    }
}

module.exports=mapObject;

function invert(obj) {
    // Returns a copy of the object where the keys have become the values and the values the keys.
    // Assume that all of the object's values will be unique and string serializable.
    // http://underscorejs.org/#invert
    if(typeof obj !=='object'  || obj==null || Array.isArray(obj)){
        throw new Error("Invalid arguments")
    }
    else{if(Object.entries(obj).length==0 ){
            return 'you entered empty object';
        }

        let newObj={};

        for(let key in obj){
        
            let str=obj[key];
        
            //obj[str]=key;

            newObj[str]=key;

        
        }return newObj;
    }
}

module.exports=invert;


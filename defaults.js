
function defaults(obj, defaultProps) {
    // Fill in undefined properties that match properties on the `defaultProps` parameter object.
    // Return `obj`.
    // http://underscorejs.org/#defaults

    if(typeof obj !=='object' || typeof defaultProps!=='object' || 
        obj==null ||  defaultProps==null||Array.isArray(defaultProps) ||Array.isArray(obj)){
        throw new Error("Invalid arguments")
    }
    else{
        if(Object.entries(obj).length==0 ||Object.entries(defaultProps).length==0){
            return 'you entered empty object'
        }
        for(let key in defaultProps){
        
            if (!(obj.hasOwnProperty(key))){

                obj[key]=defaultProps[key];
            
                return obj;;
            }
 
        }

    }
}

module.exports=defaults;